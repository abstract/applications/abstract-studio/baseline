/* Copyright (C) 2015 Tangerine Digital Studio
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

using Gtk;

namespace Baseline {
	class headerBar : Gtk.HeaderBar {
        public headerBar(baselineWindow root){
            this.title = "Baseline";
			this.show_close_button = true;
			this.destroy.connect (Gtk.main_quit);
			this.set_decoration_layout("close,maximize,minimize:");

            var buildButton = new Gtk.MenuButton();
            //newModel.show_all();
            //buildButton.set_popup(newModel);
            buildButton.set_image(new Gtk.Image.from_file ("/Users/DiGi/Dropbox/Projects/aStudio/Baseline/data/Baseline/image/play.png"));

            var deviceButton = new Gtk.Button();
            deviceButton.set_image(new Gtk.Image.from_file ("/Users/DiGi/Dropbox/Projects/aStudio/Baseline/data/Baseline/image/phone.png"));

            var runButton = new Gtk.Button();
            runButton.set_image(new Gtk.Image.from_file ("/Users/DiGi/Dropbox/Projects/aStudio/Baseline/data/Baseline/image/charge.png"));

            this.pack_end(runButton);
            this.pack_end(deviceButton);
            this.pack_end(buildButton);

        }
    }
}

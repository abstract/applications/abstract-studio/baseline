/* Copyright (C) 2015 Tangerine Digital Studio
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

namespace Baseline {
	class folderItem : AbstractLib.FileTree.Item {
		private string path;
		private baselineWindow window;
		private string data;
        private FileInfo info;

		public folderItem(baselineWindow window, string name, string path){
			this.name = name;
			this.path = path;
			this.window = window;
			this.data = data;
            var file = GLib.File.new_for_path (path);
            FileInfo info = file.query_info(GLib.FileAttribute.STANDARD_CONTENT_TYPE + "," + GLib.FileAttribute.STANDARD_IS_BACKUP + "," + GLib.FileAttribute.STANDARD_IS_HIDDEN + "," + GLib.FileAttribute.STANDARD_DISPLAY_NAME + "," + GLib.FileAttribute.STANDARD_TYPE, FileQueryInfoFlags.NONE);
            //this.icon1 = GLib.ContentType.get_icon(info.get_content_type());

			this.activated.connect(() => {
				this.setActive();
			});
		}
		public void setActive(){
			this.window.activeNotebook_i.activeList_t.add_item( new activeItem(this.window, this.name, this.path));
		}
	}
}

/* Copyright (C) 2015 Tangerine Digital Studio
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

using Gtk;
using Granite;
using WebKit;

namespace Baseline {
	class builderTab : Granite.Widgets.Tab {

		public builderList		builderList_i;

		public builderTab() {
            this.builderList_i = new builderList();

            this.page = this.builderList_i;
            this.label = "B";
		}
	}
}

/* Copyright (C) 2015 Tangerine Digital Studio
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

using Gtk;

namespace Baseline {
	class subHeader : Gtk.Toolbar {
        private baselineWindow mainWindow;
        private Gtk.StackSwitcher stackSwitcher;

        public subHeader(baselineWindow root) {
            this.mainWindow = root;

            this.get_style_context().add_class("subheader");
			this.set_icon_size(Gtk.IconSize.LARGE_TOOLBAR);

			var stackWrapper = new Gtk.ToolItem();
			this.insert(stackWrapper, 0);

			stackSwitcher = new Gtk.StackSwitcher();
			stackWrapper.add(stackSwitcher);

			Gtk.ToolItem expander = new Gtk.ToolItem();
			expander.add(new Gtk.Label(""));
			expander.set_expand(true);
			this.insert(expander, 1);

			Gtk.ToolItem sidebarSelector = new Gtk.ToolItem();
			sidebarSelector.add(this.settingsMenu());
			this.insert(sidebarSelector, 2);

			stackSwitcher.show_all();
			stackWrapper.show_all();
			this.show_all();
        }

        public void setStackSwitcher(Gtk.Stack stack){
            stackSwitcher.set_stack(stack);
        }

        private Gtk.MenuButton settingsMenu(){

            var accel_group = new Gtk.AccelGroup();
            var settingsButton = new Gtk.MenuButton();

            Gtk.Menu newModel = new Gtk.Menu ();
			newModel.set_accel_group(accel_group);

			Gtk.MenuItem item_folder = new Gtk.MenuItem.with_label ("Open Folder");
			item_folder.activate.connect(() =>{
				this.mainWindow.dialog_openFolder();
			});
			newModel.add (item_folder);

			Gtk.MenuItem item_file = new Gtk.MenuItem.with_label ("Open File");
			item_file.activate.connect(() =>{
				this.mainWindow.dialog_openFile();
			});
			newModel.add (item_file);

			Gtk.SeparatorMenuItem separator = new Gtk.SeparatorMenuItem ();
			newModel.add (separator);

			Gtk.MenuItem item_save = new Gtk.MenuItem.with_label ("Save File");
			item_save.activate.connect(() =>{
				this.mainWindow.wbview.wbview.execute_script("docSave();");
			});
			newModel.add (item_save);
			newModel.add_accelerator("activate", accel_group, 'S', Gdk.ModifierType.CONTROL_MASK, Gtk.AccelFlags.VISIBLE);

			Gtk.SeparatorMenuItem separator1 = new Gtk.SeparatorMenuItem ();
			newModel.add (separator1);

			Gtk.MenuItem item_settings = new Gtk.MenuItem.with_label ("Settings");
			item_settings.activate.connect(() =>{
				this.mainWindow.dialog_openFile();
			});
			newModel.add (item_settings);

            newModel.show_all();
            settingsButton.set_popup(newModel);
            settingsButton.set_image(new Gtk.Image.from_file ("/Users/DiGi/Dropbox/Projects/aStudio/Baseline/data/Baseline/image/settings.png"));

            return settingsButton;
        }
    }
}

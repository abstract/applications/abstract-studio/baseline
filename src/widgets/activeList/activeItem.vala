/* Copyright (C) 2015 Tangerine Digital Studio
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

namespace Baseline {
	class activeItem : AbstractLib.FileTree.Item {

		private string path;
		private baselineWindow window;
		private string data;

		public activeItem(baselineWindow window, string name, string path){
			this.name = name;
			this.path = path;
			this.window = window;
			this.data = data;

			this.activated.connect(() => {
				this.setActive();
			});
		}

		public void setActive(){
			this.window.wbview.wbview.execute_script("docOpen('"+this.path+"');");
		}

	}
}

/* Copyright (C) 2015 Tangerine Digital Studio
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

using Gtk;
using Granite;
using WebKit;

namespace Baseline {
	class deploymentTab : Granite.Widgets.Tab {

		public deploymentList		deploymentList_i;

		public deploymentTab() {
            this.deploymentList_i = new deploymentList();

            this.page = this.deploymentList_i;
            this.label = "D";
		}
	}
}

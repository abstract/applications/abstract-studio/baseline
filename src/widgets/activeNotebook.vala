/* Copyright (C) 2015 Tangerine Digital Studio
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

using Gtk;
using WebKit;

namespace Baseline {
	class activeNotebook : Gtk.Stack {

		public activeList 		activeList_t;
		public builderList 		builderList_t;
		public deploymentList 	deploymentList_t;

		public activeNotebook(baselineWindow window) {
			//this.show_tabs = true;
			//this.allow_drag = false;
			//this.allow_new_window = false;
			//this.tabs_closable = false;
			//this.add_button_visible = false;
			//this.force_left = true;

			Gtk.IconTheme.add_builtin_icon("ActiveItem", 32, new Gdk.Pixbuf.from_file("/Users/DiGi/Dropbox/Projects/aStudio/Baseline/data/Baseline/image/folder.png"));
			Gtk.IconTheme.add_builtin_icon("BuilderItem", 32, new Gdk.Pixbuf.from_file("/Users/DiGi/Dropbox/Projects/aStudio/Baseline/data/Baseline/image/hammer.png"));
			Gtk.IconTheme.add_builtin_icon("DeploymentItem", 32, new Gdk.Pixbuf.from_file("/Users/DiGi/Dropbox/Projects/aStudio/Baseline/data/Baseline/image/cloud.png"));

			Gtk.IconTheme.add_builtin_icon("ActiveItem_Active", 32, new Gdk.Pixbuf.from_file("/Users/DiGi/Dropbox/Projects/aStudio/Baseline/data/Baseline/image/folder_active.png"));
			Gtk.IconTheme.add_builtin_icon("BuilderItem_Active", 32, new Gdk.Pixbuf.from_file("/Users/DiGi/Dropbox/Projects/aStudio/Baseline/data/Baseline/image/hammer_active.png"));
			Gtk.IconTheme.add_builtin_icon("DeploymentItem_Active", 32, new Gdk.Pixbuf.from_file("/Users/DiGi/Dropbox/Projects/aStudio/Baseline/data/Baseline/image/cloud_active.png"));


            this.activeList_t = new activeList(window, "", "");
            this.add_titled(this.activeList_t,  "ActiveItem", "Files");
			this.child_set_property (this.activeList_t, "icon-name", "ActiveItem");

            this.builderList_t = new builderList(window, "", "");
            this.add_titled(this.builderList_t, "BuilderItem", "B");
			this.child_set_property (this.builderList_t, "icon-name", "BuilderItem");

            this.deploymentList_t = new deploymentList(window, "", "");
            this.add_titled(this.deploymentList_t, "DeploymentItem", "D");
			this.child_set_property (this.deploymentList_t, "icon-name", "DeploymentItem");

            this.set_visible_child(this.activeList_t);
            this.show_all();

		}
	}
}

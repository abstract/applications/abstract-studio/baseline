/* Copyright (C) 2015 Tangerine Digital Studio
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

using GLib;

namespace Baseline {
	class docManager {

		public static baselineWindow window;
		public static docManager(){
		}

		/* Root Method */
		public static void openFolder(string path, folderList folderlist, baselineWindow window){
			var file = File.new_for_path(path);
            folderlist.projectList.name = file.get_basename();
			try {
				var e = file.enumerate_children(FileAttribute.STANDARD_NAME, 0, new Cancellable());
				while(true){
					var files = e.next_file(new Cancellable());
					if (files == null) {
	                    break;
	                }
					if (files.get_file_type () == FileType.DIRECTORY) {
						//File subdir = file.resolve_relative_path (files.get_name ());
						//list_children (subdir,  + " ", cancellable);
						folderlist.projectList.add( new folderFolder(window, files.get_name (), path+"/"+files.get_name ()));
						stderr.printf("Added Folder"+path+"/"+files.get_name ()+"\n");
					} else {
						folderlist.projectList.add( new folderItem(window, files.get_name (), path+"/"+files.get_name ()));
						stderr.printf("Added File"+path+"/"+files.get_name ()+"\n");
					}
				}

			} catch (Error err){
				stderr.printf(err.message);
			}
		}

		/* When you open a Folder! */
		public static void openFolderFolder(string path, folderFolder folder, baselineWindow window){
			var file = File.new_for_path(path);
			try {
				var e = file.enumerate_children(FileAttribute.STANDARD_NAME, 0, new Cancellable());
				while(true){
					var files = e.next_file(new Cancellable());
					if (files == null) {
	                    break;
	                }
					if (files.get_file_type () == FileType.DIRECTORY) {
						folder.add( new folderFolder(window, files.get_name (), path+"/"+files.get_name ()));
						stderr.printf("Added Folder"+path+"/"+files.get_name ());
					} else {
						folder.add( new folderItem(window, files.get_name (), path+"/"+files.get_name ()));
						stderr.printf("Added File"+path+"/"+files.get_name ());
					}
				}

			} catch (Error err){
				stderr.printf(err.message);
			}
		}
	}
}

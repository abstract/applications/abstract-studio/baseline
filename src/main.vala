/* Copyright (C) 2015 Tangerine Digital Studio
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

int main (string[] args) {
    var Baseline = new Baseline.baselineWindow();
    return Baseline.run(args);
}

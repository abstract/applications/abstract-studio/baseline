/* Copyright (C) 2015 Tangerine Digital Studio
 *
 * This software may be modified and distributed under the terms
 * of the MIT license.  See the LICENSE file for details.
 */

using Gtk;
using WebKit;

namespace Baseline {
	class baselineWindow : Gtk.Application {
        /* UI Stuff */
        public Gtk.Window 		instance;
        public headerBar 	header;
        public subHeader 		subheader;
        public WebKit_View		wbview;
        public activeList		activeList_i;
        public activeNotebook	activeNotebook_i;
        public folderList		folderList_i;

		public baselineWindow() {
			docManager.window = this;
		}

		private void setupHeaderBar(){
			header = new headerBar(this);
			instance.set_titlebar(header);
			subheader = new subHeader(this);
		}


		private Gtk.Widget setupFolderView(){
			/* Add active Files Source List */
			activeNotebook_i = new activeNotebook(this);

			/* Add folder Source List */
			var oFileBox = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
			oFileBox.pack_start(activeNotebook_i, true, true);

			var folderBox = new Gtk.Paned(Gtk.Orientation.VERTICAL);
 			folderBox.add1(oFileBox);

 			folderList_i = new folderList();
 			folderBox.add2(folderList_i);

 			folderBox.set_position(100);

 			this.subheader.setStackSwitcher(activeNotebook_i);
 			return folderBox;
		}

		protected override void activate () {
            instance = new Gtk.Window();

            instance.title = "Baseline";
			instance.set_position (Gtk.WindowPosition.CENTER);
			instance.set_default_size (800, 600);
			instance.destroy.connect (Gtk.main_quit);

			this.setupHeaderBar();

			/* Build Content Box */
			var contentBox = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);

			this.wbview = new WebKit_View();

 			var mainBox = new Gtk.Paned(Gtk.Orientation.HORIZONTAL);
 			mainBox.add1(this.setupFolderView());
 			mainBox.add2(this.wbview);

 			mainBox.set_position(200);

 			contentBox.pack_start(subheader, false, false, 0);
 			contentBox.pack_end(mainBox, true, true, 1);

			instance.add(contentBox);
			/* Add that fancyass header */
			instance.set_titlebar(header);
            instance.show_all();

            /* Keep GTK In the Loop.... Eh? Eh? */
            Gtk.main();
        }

        public void dialog_openFile() {
        	var file_chooser = new FileChooserDialog ("Open File", (Gtk.Window) this,
                                      FileChooserAction.OPEN,
                                      Stock.CANCEL, ResponseType.CANCEL,
                                      Stock.OPEN, ResponseType.ACCEPT);

	        if (file_chooser.run () == ResponseType.ACCEPT) {
	            var docName = file_chooser.get_filename();
        	           // activeNotebook_i.activeList_t.activeList_i.root.add();
        	    activeNotebook_i.activeList_t.add_item(new activeItem(this, file_chooser.get_file().get_basename(), file_chooser.get_filename ()));
	        }
	        file_chooser.destroy ();
        }

        public void dialog_openFolder(){
        	var file_chooser = new FileChooserDialog ("Open Folder", (Gtk.Window) this,
                                      FileChooserAction.SELECT_FOLDER,
                                      Stock.CANCEL, ResponseType.CANCEL,
                                      Stock.OPEN, ResponseType.ACCEPT);

	        if (file_chooser.run () == ResponseType.ACCEPT) {
	        	//this.folderList_i.projectList.clear();
	            var docName = file_chooser.get_filename();
	            docManager.openFolder(docName, folderList_i, this);
	            //this.folderList_i.refilter();
	        }
	        file_chooser.destroy ();
        }
	}
}

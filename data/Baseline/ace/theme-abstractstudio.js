var cssTextCopy = ".ace-abstractstudio .ace_gutter {"+
"  background: #F7F8FA;"+
"  color: #899098"+
"}"+
""+
".ace-abstractstudio .ace_print-margin {"+
"  width: 1px;"+
"  background: #F7F8FA"+
"}"+
""+
".ace-abstractstudio {"+
"  background-color: #F7F8FA;"+
"  color: #080808"+
"}"+
""+
".ace-abstractstudio .ace_cursor {"+
"  color: #000000"+
"}"+
""+
".ace-abstractstudio .ace_marker-layer .ace_selection {"+
"  background: rgba(39, 95, 255, 0.30)"+
"}"+
""+
".ace-abstractstudio.ace_multiselect .ace_selection.ace_start {"+
"  box-shadow: 0 0 3px 0px #F9F9F9;"+
"  border-radius: 2px"+
"}"+
""+
".ace-abstractstudio .ace_marker-layer .ace_step {"+
"  background: rgb(255, 255, 0)"+
"}"+
""+
".ace-abstractstudio .ace_marker-layer .ace_bracket {"+
"  margin: -1px 0 0 -1px;"+
"  border: 1px solid rgba(75, 75, 126, 0.50)"+
"}"+
""+
".ace-abstractstudio .ace_marker-layer .ace_active-line {"+
"  background: rgba(36, 99, 180, 0.12)"+
"}"+
""+
".ace-abstractstudio .ace_gutter-active-line {"+
"  background-color : #dcdcdc"+
"}"+
""+
".ace-abstractstudio .ace_marker-layer .ace_selected-word {"+
"  border: 1px solid rgba(39, 95, 255, 0.30)"+
"}"+
""+
".ace-abstractstudio .ace_invisible {"+
"  color: rgba(75, 75, 126, 0.50)"+
"}"+
""+
".ace-abstractstudio .ace_keyword,"+
".ace-abstractstudio .ace_meta {"+
"  color: #247BFA"+
"}"+
""+
".ace-abstractstudio .ace_constant,"+
".ace-abstractstudio .ace_constant.ace_character,"+
".ace-abstractstudio .ace_constant.ace_character.ace_escape,"+
".ace-abstractstudio .ace_constant.ace_other {"+
"  color: #811F24"+
"}"+
""+
".ace-abstractstudio .ace_invalid.ace_illegal {"+
"  text-decoration: underline;"+
"  font-style: italic;"+
"  color: #F8F8F8;"+
"  background-color: #B52A1D"+
"}"+
""+
".ace-abstractstudio .ace_invalid.ace_deprecated {"+
"  text-decoration: underline;"+
"  font-style: italic;"+
"  color: #B52A1D"+
"}"+
""+
".ace-abstractstudio .ace_support {"+
"  color: #F14D77"+
"}"+
""+
".ace-abstractstudio .ace_support.ace_constant {"+
"  color: #BD9FDC"+
"}"+
""+
".ace-abstractstudio .ace_fold {"+
"  background-color: #794938;"+
"  border-color: #080808"+
"}"+
""+
".ace-abstractstudio .ace_list,"+
".ace-abstractstudio .ace_markup.ace_list,"+
".ace-abstractstudio .ace_support.ace_function {"+
"  color: #693A17"+
"}"+
""+
".ace-abstractstudio .ace_storage {"+
"  font-style: italic;"+
"  color: #36B299"+
"}"+
""+
".ace-abstractstudio .ace_string {"+
"  color: #0B6125"+
"}"+
""+
".ace-abstractstudio .ace_string.ace_regexp {"+
"  color: #CF5628"+
"}"+
""+
".ace-abstractstudio .ace_comment {"+
"  font-style: italic;"+
"  color: #5F6266"+
"}"+
".ace-abstractstudio .ace_identifier {"+

"}"+
""+
".ace-abstractstudio .ace_heading,"+
".ace-abstractstudio .ace_markup.ace_heading {"+
"  color: #19356D"+
"}"+
""+
".ace-abstractstudio .ace_variable {"+
"  color: #234A97"+
"}"+
""+
".ace-abstractstudio .ace_indent-guide {"+
"  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEklEQVQImWNgYGBgYLh/5+x/AAizA4hxNNsZAAAAAElFTkSuQmCC) right repeat-y"+
"}";

define("ace/theme/abstractstudio",["require","exports","module","ace/lib/dom"],function(e,t,n){t.isDark=!1,t.cssClass="ace-abstractstudio",t.cssText=cssTextCopy;var r=e("../lib/dom");r.importCssString(t.cssText,t.cssClass)})